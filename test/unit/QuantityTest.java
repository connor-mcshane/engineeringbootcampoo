/*
 * Copyright (c) 2017 by Fred George
 * May be used freely except for training; license required for training.
 */

package unit;

import org.junit.jupiter.api.Test;
import quantity.IntervalQuantity;

import java.util.Arrays;
import java.util.HashSet;

import static quantity.Unit.*;

import static org.junit.jupiter.api.Assertions.*;

// Ensures quantity.Quantity operates correctly
class QuantityTest {

    @Test void equalityOfLikeUnits() {
        assertEquals(TABLESPOON.s(8), TABLESPOON.s(8));
        assertNotEquals(TABLESPOON.s(8), TABLESPOON.s(6));
        assertNotEquals(TABLESPOON.s(8), new Object());
        assertNotEquals(TABLESPOON.s(8), null);
    }

    @Test void equalityOfDifferentUnits() {
        assertNotEquals(TABLESPOON.s(8), CUP.s(8));
        assertEquals(TABLESPOON.s(8), CUP.s(0.5));
        assertEquals(CUP.s(1/4.0), TABLESPOON.s(4));
        assertEquals(GALLON.s(1), TEASPOON.s(768));
        assertEquals(FOOT.s(330), FURLONG.s(0.5));
        assertEquals(MILE.s(1), INCH.es(12 * 5280));
    }

    @Test void polymorphism() {
        assertEquals(1, new HashSet<>(Arrays.asList(TABLESPOON.s(8), TABLESPOON.s(8))).size());
        assertEquals(1, new HashSet<>(Arrays.asList(TABLESPOON.s(8), CUP.s(0.5))).size());
    }

    @Test void hash() {
        assertEquals(TABLESPOON.s(8).hashCode(), TABLESPOON.s(8).hashCode());
        assertEquals(TABLESPOON.s(8).hashCode(), CUP.s(0.5).hashCode());
        assertEquals(FOOT.s(330).hashCode(), FURLONG.s(0.5).hashCode());
        assertEquals(CELSIUS.es(10).hashCode(), FAHRENHEIT.s(50).hashCode());
    }

    @Test void crossMetricType()  {
        assertNotEquals(INCH.es(1), TEASPOON.s(1));
        assertNotEquals(OUNCE.s(4), FOOT.s(2));
    }

    @Test void arithmetic() {
        assertEquals(QUART.s(0.5), TABLESPOON.s(6).plus(OUNCE.s(13)));
        assertEquals(TABLESPOON.s(-6), TABLESPOON.s(6).negate());
        assertEquals(PINT.s(-0.5), TABLESPOON.s(10).minus(OUNCE.s(13)));
    }

    @Test void mixedUnitArithmetic() {
        assertThrows(IllegalArgumentException.class, () ->
                YARD.s(3).minus(TABLESPOON.s(4)));
    }

    @Test void temperature() {
        assertSymmetricEquals(CELSIUS.es(0), FAHRENHEIT.s(32));
        assertSymmetricEquals(CELSIUS.es(-40), FAHRENHEIT.s(-40));
        assertSymmetricEquals(CELSIUS.es(10), FAHRENHEIT.s(50));
        assertSymmetricEquals(CELSIUS.es(100), FAHRENHEIT.s(212));
    }

    private void assertSymmetricEquals(IntervalQuantity left, IntervalQuantity right) {
        assertEquals(left, right);
        assertEquals(right, left);
    }

    @Test void temperatureArithmetic() {
        // The following should not compile!
//        CELSIUS.es(10).minus(FAHRENHEIT.s(32));
    }
}