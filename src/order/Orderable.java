package order;

import java.util.stream.Stream;

// Understands a sequence
public interface Orderable<T>  {
    boolean isBetterThan(T other);

    static <T extends Orderable<T>> T best(T ... elements) {
        if (elements.length == 0) return null;
        return Stream.of(elements).reduce(elements[0],
                (champion, challenger) -> challenger.isBetterThan(champion) ? challenger : champion);
    }
}
