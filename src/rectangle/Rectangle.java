/*
 * Copyright (c) 2017 by Fred George
 * May be used freely except for training; license required for training.
 */

package rectangle;

import order.Orderable;

// Understands a four-sided polygon with sides at right angles
public class Rectangle implements Orderable<Rectangle> {
    private final double height;
    private final double width;

    public Rectangle(double height, double width) {
        if (height <= 0.0 || width <= 0.0)
            throw new IllegalArgumentException("Dimensions must be greater than zero");
        this.height = height;
        this.width = width;
    }

    public static Rectangle square(double side) { return new Rectangle(side, side); }

    public double area() {
        return height * width;
    }

    public double perimeter() {
        return 2 * (height + width);
    }

    @Override
    public boolean isBetterThan(Rectangle other) {
        return this.area() > other. area();
    }
}
