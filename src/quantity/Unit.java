/*
 * Copyright (c) 2017 by Fred George
 * May be used freely except for training; license required for training.
 */

package quantity;

// Understands a specific metric
public class Unit {
    public final static RatioUnit TEASPOON = new RatioUnit();
    public final static RatioUnit TABLESPOON = new RatioUnit(3, TEASPOON);
    public final static RatioUnit OUNCE = new RatioUnit(2, TABLESPOON);
    public final static RatioUnit CUP = new RatioUnit(8, OUNCE);
    public final static RatioUnit PINT = new RatioUnit(2, CUP);
    public final static RatioUnit QUART = new RatioUnit(2, PINT);
    public final static RatioUnit GALLON = new RatioUnit(4, QUART);

    public final static RatioUnit INCH = new RatioUnit();
    public final static RatioUnit FOOT = new RatioUnit(12, INCH);
    public final static RatioUnit YARD = new RatioUnit(3, FOOT);
    public final static RatioUnit CHAIN = new RatioUnit(22, YARD);
    public final static RatioUnit FURLONG = new RatioUnit(10, CHAIN);
    public final static RatioUnit MILE = new RatioUnit(8, FURLONG);

    public final static Unit CELSIUS = new Unit();
    public final static Unit FAHRENHEIT = new Unit(5/9.0, 32, CELSIUS);

    private final Unit baseUnit;
    private final double baseUnitRatio;
    private final double offset;

    private Unit() {
        baseUnit = this;
        baseUnitRatio = 1.0;
        offset = 0.0;
    }

    private Unit(double relativeRatio, double offset, Unit relativeUnit) {
        baseUnit = relativeUnit.baseUnit;
        baseUnitRatio = relativeRatio * relativeUnit.baseUnitRatio;
        this.offset = offset;
    }

    public IntervalQuantity s(double amount) {
        return new IntervalQuantity(amount, this);
    }

    public IntervalQuantity es(double amount) {
        return s(amount);
    }

    double convertedAmount(double otherAmount, Unit other) {
        if (!this.isCompatible(other)) throw new IllegalArgumentException("Incompatible units");
        return (otherAmount - other.offset) * other.baseUnitRatio / this.baseUnitRatio + this.offset;
    }

    int hashCode(double amount) {
        return Double.hashCode((amount - offset) * baseUnitRatio);
    }

    boolean isCompatible(Unit other) {
        return this.baseUnit == other.baseUnit;
    }

    public static class RatioUnit extends Unit {
        RatioUnit() { super(); }
        RatioUnit(double relativeRatio, Unit relativeUnit) { super(relativeRatio, 0.0, relativeUnit); }
        public RatioQuantity s(double amount) {
            return new RatioQuantity(amount, this);
        }
        public RatioQuantity es(double amount) {
            return s(amount);
        }
    }
}
